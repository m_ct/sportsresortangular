import { TeamMember } from './team.interface';

export interface Sport {
  id: number;
  idUser: string;
  date: number;
  sport: string;
  value: number;
  tdClass?: string;
}

export interface Sportsman {
  id: string;
  mail: string;
  pass: string;
  sportsmanName: string;
  location: string;
  date: number;
  thoughts: string;
  loggedIn: boolean;
  sports: Sport[];
  total: number;
  rank: number;
  tdClass?: string;
}

export interface DataDef {
  key: string;
  label: string;
  datatype: string;
  visible: boolean;
  searchable: boolean;
}

export interface SportsmenPage {
  headers: DataDef[];
  users: Sportsman[];
  total: number;
  page: number;
  pageSize: number;
}

export interface SportsPage {
  headers: DataDef[];
  data: Sport[];
  total: number;
  page: number;
  pageSize: number;
}

export interface TeamPage {
  headers: DataDef[];
  data: TeamMember[];
  total: number;
  page: number;
  pageSize: number;
}
