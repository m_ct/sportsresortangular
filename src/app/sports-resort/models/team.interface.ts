export interface GalleryItem {
  id: number;
  key: string;
  description: string;
  image: string;
}

export interface TeamMember {
  id: number;
  name: string;
  title: string;
  department: string;
  about: string;
  image: string;
  tdClass?: string;
}
