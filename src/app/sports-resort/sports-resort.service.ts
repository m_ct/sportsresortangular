import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  Sportsman,
  SportsmenPage,
  Sport,
  SportsPage,
  TeamPage,
} from './models/sportsman.interface';
import { TeamMember } from './models/team.interface';
import { GalleryItem } from './models/team.interface';

import { Observable, tap, of, catchError, concatMap } from 'rxjs';
import { map } from 'rxjs/operators';

const SPORTSMEN_API: string = '/api/SportsData';
const SPORTSMEN_HEADERS_API: string = '/api/SportsDataDef';
const SPORTS_API: string = '/api/Sports';
const SPORTS_HEADERS_API: string = '/api/SportsDef';
const TEAM_API: string = '/api/TeamData';
const GALLERY_API: string = '/api/GalleryData';

@Injectable()
export class SportsResortService {
  constructor(private http: HttpClient) {}

  getSportsmen(): Observable<Sportsman[]> {
    return this.http.get<Sportsman[]>(SPORTSMEN_API).pipe(
      //tap((x) => console.log('Before map', x)),
      map((res: Sportsman[]) => res),
      catchError(this.handleError<Sportsman[]>(`all users`))
    );
  }

  getSportsmenPage(
    page: number,
    pageSize: number,
    sortColumn: string,
    sortDirection: string,
    filter: string
  ): Observable<SportsmenPage> {
    let urlParam = `?`;
    //1. filter first

    let params = '';

    if (filter.length > 0) {
      params = params.concat(`location_like=${filter}`);
    }

    //2. sort
    if (sortColumn.length > 0) {
      if (params.length > 0 && sortDirection.length > 0) {
        params = params.concat(`&`);
      }
      if (sortDirection.length > 0) {
        params = params.concat(`_sort=${sortColumn}&_order=${sortDirection}`);
      }
    }

    //3. paginate
    if (params.length > 0) {
      params = params.concat(`&`);
    }
    params = params.concat(`_page=${page}&_limit=${pageSize}`);

    urlParam = urlParam.concat(params);

    console.log('URL: ', urlParam);

    //get the headers
    return this.http.get<any>(SPORTSMEN_HEADERS_API).pipe(
      //tap((res) => console.log('First result', res)),
      concatMap((data: any) =>
        this.http
          .get<any>(`${SPORTSMEN_API}/${urlParam}`, { observe: 'response' })
          .pipe(
            tap((x) => console.log('sportsmenpage 1 ', x)),
            map((res: any) => {
              //console.log(res.headers.get('x-total-count'));

              //console.log('data headers', data);

              return {
                headers: data,
                users: res.body,
                total: res.headers.get('x-total-count'),
                page: page,
                pageSize: pageSize,
              };
            })
            //tap((x) => {
            //  console.log('sportsmenpage 2 ', x);
            //  console.log(x);
            //})
          )
      )
      //tap((x) => console.log('last result ', x))
    );
  }

  getSportsPage(
    page: number,
    pageSize: number,
    sortColumn: string,
    sortDirection: string,
    filter: string
  ): Observable<SportsPage> {
    let par = '?loggedIn=true';
    const url = `${SPORTSMEN_API}/${par}`;

    return this.http.get<Sportsman[]>(url).pipe(
      //tap((x) => console.log('Before map', x)),
      concatMap((res: Sportsman[]) => {
        //console.log(res[0]);

        let urlParam = `?idUser=${res[0].id}&`;
        let params = '';

        //1. filter first
        if (filter.length > 0) {
          params = params.concat(`sport_like=${filter}`);
        }

        //2. sort
        if (sortColumn.length > 0) {
          if (params.length > 0 && sortDirection.length > 0) {
            params = params.concat(`&`);
          }
          if (sortDirection.length > 0) {
            params = params.concat(
              `_sort=${sortColumn}&_order=${sortDirection}`
            );
          }
        }

        //3. paginate
        if (params.length > 0) {
          params = params.concat(`&`);
        }
        params = params.concat(`_page=${page}&_limit=${pageSize}`);

        urlParam = urlParam.concat(params);

        //console.log('URL: ', urlParam);

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //get the headers
        return this.http.get<any>(SPORTS_HEADERS_API).pipe(
          //tap((head) => console.log('First result', head)),
          concatMap((data: any) =>
            this.http
              .get<any>(`${SPORTS_API}/${urlParam}`, { observe: 'response' })
              .pipe(
                //tap((x) => console.log('sports 1 ', x)),
                map((res: any) => {
                  //console.log(res.headers.get('x-total-count'));

                  //console.log('data headers', data);
                  //console.log('body', res.body);

                  return {
                    headers: data,
                    data: res.body,
                    total: res.headers.get('x-total-count'),
                    page: page,
                    pageSize: pageSize,
                  };
                })
                //tap((x) => {
                //  console.log('sportsmenpage 2 ', x);
                //  console.log(x);
                //})
              )
          )
          //tap((x) => console.log('last result ', x))
        );
      })
    );
  }

  getLastSport(): Observable<Sport[]> {
    let sortColumn = 'id';
    let sortDirection = 'desc';
    let page = 1;
    let pageSize = 1;

    let urlParam = `?`;
    let params = '';
    params = params.concat(`_sort=${sortColumn}&_order=${sortDirection}&`);
    params = params.concat(`_page=${page}&_limit=${pageSize}`);
    urlParam = urlParam.concat(params);

    console.log('URL: ', urlParam);

    return this.http.get<Sport[]>(`${SPORTS_API}/${urlParam}`).pipe(
      map((res: Sport[]) => res),
      catchError(this.handleError<Sport[]>(`last sport activity`))
    );
  }

  addSport(data: Sport): Observable<Sport> {
    const url = `${SPORTS_API}`;

    return this.http.post<Sport>(url, data).pipe(
      //tap((x) => console.log('Before POST map', x)),
      map((res: Sport) => res)
    );
  }

  deleteSport(id: number): Observable<Sport> {
    return this.http
      .delete<Sport>(`${SPORTS_API}/${id}`)
      .pipe(map((res: Sport) => res));
  }

  getUser(paramMail: string, paramPass: string): Observable<Sportsman[]> {
    let urlParam = `?mail=${paramMail}&pass=${paramPass}`;
    return this.http.get<Sportsman[]>(`${SPORTSMEN_API}/${urlParam}`).pipe(
      map((res: Sportsman[]) => res),
      catchError(this.handleError<Sportsman[]>(`user mail=${paramMail}`))
    );
  }

  getTopUsers(): Observable<Sportsman[]> {
    let sortColumn = 'total';
    let sortDirection = 'desc';
    let page = 1;
    let pageSize = 3;

    let urlParam = `?`;
    let params = '';
    params = params.concat(`_sort=${sortColumn}&_order=${sortDirection}&`);
    params = params.concat(`_page=${page}&_limit=${pageSize}`);
    urlParam = urlParam.concat(params);

    console.log('URL: ', urlParam);
    return this.http.get<Sportsman[]>(`${SPORTSMEN_API}/${urlParam}`).pipe(
      map((res: Sportsman[]) => res),
      catchError(this.handleError<Sportsman[]>(`top users`))
    );
  }

  getRankedUsers(): Observable<Sportsman[]> {
    let sortColumn = 'rank';
    let sortDirection = 'desc';
    let page = 1;
    let pageSize = 3;

    let urlParam = `?`;
    let params = '';
    params = params.concat(`_sort=${sortColumn}&_order=${sortDirection}&`);
    params = params.concat(`_page=${page}&_limit=${pageSize}`);
    urlParam = urlParam.concat(params);

    console.log('URL: ', urlParam);
    return this.http.get<Sportsman[]>(`${SPORTSMEN_API}/${urlParam}`).pipe(
      map((res: Sportsman[]) => res),
      catchError(this.handleError<Sportsman[]>(`top users`))
    );
  }

  getUserSports(paramId: string): Observable<Sport[]> {
    let urlParam = `?idUser=${paramId}`;
    return this.http
      .get<Sport[]>(`${SPORTS_API}/${urlParam}`)
      .pipe(map((res: Sport[]) => res));
  }

  getAllSports(): Observable<Sport[]> {
    return this.http
      .get<Sport[]>(`${SPORTS_API}`)
      .pipe(map((res: Sport[]) => res));
  }

  getLoggedInUser(): Observable<Sportsman[]> {
    let urlParam = '?loggedIn=true';
    const url = `${SPORTSMEN_API}/${urlParam}`;
    return this.http.get<Sportsman[]>(url).pipe(
      //tap((x) => console.log('Before map', x)),
      map((res: Sportsman[]) => res),
      catchError(this.handleError<Sportsman[]>(`logged in user`))
    );
  }

  updateUser(sportsman: Sportsman): Observable<Sportsman> {
    if (sportsman) {
      const url2 = `${SPORTSMEN_API}/${sportsman.id}`;

      return this.http.put<Sportsman>(url2, sportsman).pipe(
        //tap((x) => console.log('Before PUT map', x)),
        map((res: Sportsman) => res),
        catchError(this.handleError<Sportsman>(`user id=${sportsman.id}`))
      );
    } else {
      var sp: Sportsman = <Sportsman>{};
      return of(sp);
    }
  }

  addUser(sportsman: Sportsman): Observable<Sportsman> {
    if (sportsman.id == undefined) {
      return of(<Sportsman>{});
    } else {
      const url = `${SPORTSMEN_API}`;

      return this.http.post<Sportsman>(url, sportsman).pipe(
        //tap((x) => console.log('Before POST map', x)),
        map((res: Sportsman) => res),
        catchError(this.handleError<Sportsman>(`user id=${sportsman.id}`))
      );
    }
  }

  deleteUser(sportsman: Sportsman): Observable<Sportsman> {
    if (sportsman) {
      return this.http
        .delete<Sportsman>(`${SPORTSMEN_API}/${sportsman.id}`)
        .pipe(
          map((res: Sportsman) => res),
          catchError(this.handleError<Sportsman>(`user id=${sportsman.id}`))
        );
    } else {
      var sp: Sportsman = <Sportsman>{};
      return of(sp);
    }
  }

  getTeam(): Observable<TeamMember[]> {
    return this.http.get<TeamMember[]>(TEAM_API).pipe(
      map((res: TeamMember[]) => res),
      catchError(this.handleError<TeamMember[]>(`all team members`))
    );
  }

  getTeamPage(
    page: number,
    pageSize: number,
    sortColumn: string,
    sortDirection: string,
    filter: string
  ): Observable<TeamPage> {
    let urlParam = `?`;
    //1. filter first

    let params = '';

    if (filter.length > 0) {
      params = params.concat(`name_like=${filter}`);
    }

    //2. sort
    if (sortColumn.length > 0) {
      if (params.length > 0 && sortDirection.length > 0) {
        params = params.concat(`&`);
      }
      if (sortDirection.length > 0) {
        params = params.concat(`_sort=${sortColumn}&_order=${sortDirection}`);
      }
    }

    //3. paginate
    if (params.length > 0) {
      params = params.concat(`&`);
    }
    params = params.concat(`_page=${page}&_limit=${pageSize}`);

    urlParam = urlParam.concat(params);

    console.log('URL: ', urlParam);

    return this.http
      .get<any>(`${TEAM_API}/${urlParam}`, { observe: 'response' })
      .pipe(
        map((res: any) => {
          console.log('server', res, res.headers.get('x-total-count'));
          return {
            headers: <any>[],
            data: res.body,
            total: res.headers.get('x-total-count'),
            page: page,
            pageSize: pageSize,
          };
        })
      );

    /*
      .pipe(
        tap((x) => console.log('team 1 ', x)),
        map((res: any) => {
          //console.log(res.headers.get('x-total-count'));

          //console.log('data headers', data);

          return {
            headers: [],
            users: res.body,
            total: res.headers.get('x-total-count'),
            page: page,
            pageSize: pageSize,
          };
        })
        //tap((x) => {
        //  console.log('sportsmenpage 2 ', x);
        //  console.log(x);
        //})
      );
      */
  }

  getGalleryItems(key: string): Observable<GalleryItem[]> {
    let url = `${GALLERY_API}`;
    if (key.length > 0) {
      url = `${GALLERY_API}/?key=${key}`;
    }
    //console.log(url);
    return this.http.get<GalleryItem[]>(url).pipe(
      map((res: GalleryItem[]) => res),
      catchError(this.handleError<GalleryItem[]>(`gallery items`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
