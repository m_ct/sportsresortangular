import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ViewEncapsulation, ViewChild } from '@angular/core';
import {
  NgbSlideEvent,
  NgbSlideEventSource,
  NgbCarousel,
} from '@ng-bootstrap/ng-bootstrap';
import { Sportsman, Sport } from '../../models/sportsman.interface';
import { Router } from '@angular/router';

interface SportResource {
  name: string;
  image: string;
}

@Component({
  selector: 'activity-card',
  templateUrl: 'activity-card.component.html',
  styleUrls: ['activity-card.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ActivityCardComponent implements OnInit {
  @Input()
  detail: Sportsman;

  @Output()
  saveActivity: EventEmitter<any> = new EventEmitter<any>();

  images: SportResource[] = [
    { name: 'Running', image: '/assets/running.jfif' },
    { name: 'Basketball', image: '/assets/basketball.jpg' },
    { name: 'Cycling', image: '/assets/cycling.jfif' },
  ];

  showNavigationArrows = true;
  showNavigationIndicators = true;

  currentActivity: Sport = <Sport>{};

  @ViewChild('sportCarousel', { static: true }) carousel: NgbCarousel;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.currentActivity.sport = this.images[0].name;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    let slideNo = Number(slideEvent.current.substring(10)) % 3;
    switch (slideNo) {
      case 0:
        this.currentActivity.sport = this.images[0].name;
        break;
      case 1:
        this.currentActivity.sport = this.images[1].name;
        break;
      case 2:
        this.currentActivity.sport = this.images[2].name;
        break;
      default:
        this.currentActivity.sport = this.images[0].name;
        break;
    }
  }

  onClose() {
    this.router.navigate(['/sportscard']);
  }

  addActivity() {
    console.log('Emitting new activity: ' + this.currentActivity.sport);
    this.saveActivity.emit(this.currentActivity);
  }

  onValueChange(value: any) {
    this.currentActivity.value = value.valueOf();
  }

  onDateChange(value: any) {
    this.currentActivity.date = value;
  }
}
