import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sports-resort-header',
  templateUrl: './sports-resort-header.component.html',
  styleUrls: ['sports-resort-header.component.css'],
})
export class SportsResortHeader implements OnInit {
  public isMenuCollapsed = true;
  constructor() {}

  ngOnInit(): void {}
}
