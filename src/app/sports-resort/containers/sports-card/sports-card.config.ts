export class SportsCardTableConfig {
  //static SportsCardTableConfig: any[];
  static ColumnDefinition(): any[] {
    return [
      {
        key: 'sport',
        label: 'Sport',
        dataType: 'string',
        visible: true,
        searchable: true,
        thClass: ['filterSport'],
        tdClass: 'filterSport',
      },
      {
        key: 'date',
        label: 'Activity Date',
        dataType: 'date',
        visible: true,
        searchable: false,
      },
      {
        key: 'value',
        label: 'Km',
        dataType: 'number',
        visible: false,
        searchable: false,
      },
    ];
  }
}
