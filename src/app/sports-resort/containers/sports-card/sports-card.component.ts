import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import {
  Sportsman,
  SportsmenPage,
  SportsPage,
  Sport,
} from '../../models/sportsman.interface';

import { SportsResortService } from '../../sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SearchConfig } from 'src/app/shared/dynamic-table/dynamic-table.component';
import { concatMap } from 'rxjs';
import { map } from 'rxjs';
import { SportsCardTableConfig } from './sports-card.config';

@Component({
  selector: 'sports-card',
  templateUrl: 'sports-card.component.html',
  styleUrls: ['sports-card.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SportsCardComponent implements OnInit {
  currentUser: Sportsman = <Sportsman>{};

  lastSearchParams: SearchConfig = {
    page: 1,
    pageSize: 4,
    sortColumn: '',
    sortDirection: '',
    searchTerm: '',
  };

  tableData: SportsPage = <SportsPage>{};
  canDelete = true;
  searchColumn = '';

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    this.tableData.headers = SportsCardTableConfig.ColumnDefinition();
    this.sportsService.getLoggedInUser().subscribe((data: Sportsman[]) => {
      //console.log('Subscribe: ', data);
      this.currentUser = data[0];
      console.log('Current user', this.currentUser);
      return;
    });

    ////////////
    // get table data

    this.sportsService
      .getSportsPage(
        this.lastSearchParams.page,
        this.lastSearchParams.pageSize,
        this.lastSearchParams.sortColumn,
        this.lastSearchParams.sortDirection,
        this.lastSearchParams.searchTerm
      )
      .subscribe((data: SportsPage) => {
        this.tableData.data = data.data;
        this.tableData.page = data.page;
        this.tableData.pageSize = data.pageSize;
        this.tableData.total = data.total;
      });

    let searchColumns = SportsCardTableConfig.ColumnDefinition().filter(
      (item) => item.searchable
    );
    if (searchColumns.length > 0) {
      this.searchColumn = searchColumns[0].label;
    }

    console.log('received data ', this.tableData);
  }

  onDeleteData(event: any) {
    let deletedId = 0;
    this.tableData.data.forEach((element, index) => {
      if (element.date == event.date && element.sport == event.sport) {
        deletedId = element.id;
      }
    });

    let sum: number = 0;
    this.sportsService
      .getUserSports(this.currentUser.id)
      .pipe(
        concatMap((res1: any) => {
          res1.forEach((a: any) => {
            if (a.id != deletedId) {
              sum += parseInt(a.value.toString());
            }
          });
          this.currentUser.total = sum;
          return this.sportsService.deleteSport(deletedId);
        }),
        concatMap((res2: any) => {
          console.log(res2);
          return this.sportsService.updateUser(this.currentUser);
        }),
        concatMap((resF: any) => {
          return this.sportsService.getSportsPage(
            this.lastSearchParams.page,
            this.lastSearchParams.pageSize,
            this.lastSearchParams.sortColumn,
            this.lastSearchParams.sortDirection,
            this.lastSearchParams.searchTerm
          );
        })
      )
      .subscribe((data: SportsPage) => {
        this.tableData.data = data.data;
        this.tableData.page = data.page;
        this.tableData.pageSize = data.pageSize;
        this.tableData.total = data.total;
      });
  }

  onClose() {
    // rank
    let topUsers: any = [];
    this.sportsService
      .getRankedUsers()
      .pipe(
        map((res: Sportsman[]) => {
          topUsers = res;
          console.log('top users ', topUsers);
          topUsers[0].rank = 0;
          topUsers[1].rank = 0;
          topUsers[2].rank = 0;
          console.log('top users ', topUsers);
        }),
        concatMap((res1: any) => {
          console.log(res1);
          return this.sportsService.updateUser(topUsers[0]);
        }),
        concatMap((res2: any) => {
          console.log(res2);
          return this.sportsService.updateUser(topUsers[1]);
        }),
        concatMap((res3: any) => {
          console.log(res3);
          return this.sportsService.updateUser(topUsers[2]);
        }),
        //////////////////////////
        concatMap((res4: any) => {
          return this.sportsService.getTopUsers().pipe(
            map((res: Sportsman[]) => {
              topUsers = res;
              console.log('top users ', topUsers);
              topUsers[0].rank = 1;
              topUsers[1].rank = 2;
              topUsers[2].rank = 3;
              console.log('top users ', topUsers);
            }),
            concatMap((res1: any) => {
              console.log(res1);
              return this.sportsService.updateUser(topUsers[0]);
            }),
            concatMap((res2: any) => {
              console.log(res2);
              return this.sportsService.updateUser(topUsers[1]);
            }),
            concatMap((res3: any) => {
              console.log(res3);
              return this.sportsService.updateUser(topUsers[2]);
            })
          );
        })
      )
      .subscribe((d: any) => {
        console.log('last-', d);
        return;
      });

    this.router.navigate(['/sportsresort']);
  }

  addActivity() {
    this.router.navigate(['/activity']);
  }

  onChangePage(searchParams: SearchConfig) {
    this.lastSearchParams = Object.assign({}, searchParams);
    this.sportsService
      .getSportsPage(
        searchParams.page,
        searchParams.pageSize,
        searchParams.sortColumn,
        searchParams.sortDirection,
        searchParams.searchTerm
      )
      .subscribe((data: SportsPage) => {
        this.tableData.data = data.data;
        this.tableData.page = data.page;
        this.tableData.pageSize = data.pageSize;
        this.tableData.total = data.total;
      });
  }
}
