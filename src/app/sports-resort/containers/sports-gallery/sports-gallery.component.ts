import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { SportsResortService } from '../../sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GalleryItem } from '../../models/team.interface';
import {
  BehaviorSubject,
  Observable,
  of,
  Subject,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs';

@Component({
  selector: 'sports-gallery',
  templateUrl: 'sports-gallery.component.html',
  styleUrls: ['sports-gallery.component.css'],
})
export class SportsGalleryComponent implements OnInit {
  offCanvasActive: boolean = false;

  galleryItems: GalleryItem[];

  galleryItems$: Observable<GalleryItem[]>;
  private searchTerms = new BehaviorSubject<string>('');

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    /*
    this.sportsService
      .getGalleryItems('')
      .subscribe((data: GalleryItem[]) => (this.galleryItems = data));
*/
    this.galleryItems$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.sportsService.getGalleryItems(term))
    );
    //this.searchTerms.next('');
  }

  search(term: string): void {
    /*
    this.sportsService
      .getGalleryItems(term)
      .subscribe((data: GalleryItem[]) => (this.galleryItems = data));
*/
    this.searchTerms.next(term);
  }

  toggleOffCanvas() {
    this.offCanvasActive = !this.offCanvasActive;
    console.log('canvas active ' + this.offCanvasActive);
  }
}
