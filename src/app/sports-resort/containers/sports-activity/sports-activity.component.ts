import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Sportsman, Sport } from '../../models/sportsman.interface';
import { SportsResortService } from '../../sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { concatMap, tap } from 'rxjs';
//import { map } from 'rxjs';

@Component({
  selector: 'sports-activity',
  templateUrl: 'sports-activity.component.html',
  styleUrls: ['sports-activity.component.css'],
})
export class SportsActivityComponent implements OnInit {
  currentUser: Sportsman;
  currentUserSports: Sport[] = [];

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    this.sportsService
      .getLoggedInUser()
      .pipe(
        concatMap((res: Sportsman[]) => {
          console.log(res[0].id);
          this.currentUser = res[0];
          return this.sportsService.getUserSports(res[0].id);
        })
      )
      .subscribe((data: Sport[]) => {
        console.log(data);
        this.currentUserSports = data;
      });
  }

  addData(event: Sport) {
    console.log('add ', event);
    // first get next id

    let newSport: Sport = {
      id: 0,
      idUser: this.currentUser.id,
      sport: event.sport,
      date: event.date,
      value: event.value,
    };

    this.sportsService
      .getLastSport()
      .pipe(
        concatMap((res: Sport[]) => {
          console.log(res);
          newSport.id = parseInt(res[0].id.toString()) + 1;

          if (this.currentUserSports && this.currentUserSports.length > 0) {
            this.currentUserSports.forEach((element, index) => {
              if (element.date == event.date && element.sport == event.sport) {
                newSport.id = element.id;
                this.currentUserSports.splice(index, 1);
              }
            });
          }
          // add to db
          let sum: number = 0;
          this.currentUserSports.forEach(
            (a) => (sum += parseInt(a.value.toString()))
          );
          this.currentUser.total = sum + parseInt(event.value.toString());

          return this.sportsService.updateUser(this.currentUser);
        })
      )
      .subscribe((res2: Sportsman) => {
        console.log(res2);
      });

    this.sportsService.addSport(newSport).subscribe((sp: Sport) => {
      return;
    });

    this.toastr.success('Activity saved!');
    return;
  }
}
