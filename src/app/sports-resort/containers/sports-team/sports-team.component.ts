import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TeamMember } from '../../models/team.interface';
import { SportsResortService } from '../../sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SearchConfig } from 'src/app/shared/dynamic-table/dynamic-table.component';
import { TeamPage } from '../../models/sportsman.interface';

@Component({
  selector: 'sports-team',
  templateUrl: 'sports-team.component.html',
  styleUrls: ['sports-team.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SportsTeamComponent implements OnInit {
  teamMembers: TeamMember[];

  tableData: TeamPage = <TeamPage>{};
  searchColumn = '';
  columnDefinitions: any[];

  ColumnDefinition: any = [
    {
      key: 'name',
      label: 'Name',
      dataType: 'string',
      visible: true,
      searchable: true,
    },
    {
      key: 'title',
      label: 'Job Title',
      dataType: 'string',
      visible: true,
      searchable: false,
    },
    {
      key: 'department',
      label: 'Department',
      dataType: 'string',
      visible: true,
      searchable: false,
    },
    {
      key: 'about',
      label: 'About',
      dataType: 'string',
      visible: true,
      searchable: false,
      tdClass: 'italic',
    },
    {
      key: 'image',
      label: 'Image',
      dataType: 'image',
      visible: false,
      searchable: false,
    },
  ];

  lastSearchParams: SearchConfig = {
    page: 1,
    pageSize: 4,
    sortColumn: '',
    sortDirection: '',
    searchTerm: '',
  };

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    this.sportsService.getTeam().subscribe((data: TeamMember[]) => {
      this.teamMembers = data;

      console.log('team members', this.teamMembers);
      return;
    });

    this.sportsService
      .getTeamPage(
        this.lastSearchParams.page,
        this.lastSearchParams.pageSize,
        this.lastSearchParams.sortColumn,
        this.lastSearchParams.sortDirection,
        this.lastSearchParams.searchTerm
      )
      .subscribe((data: TeamPage) => {
        console.log('data from server', data.data);

        this.tableData.data = data.data;
        this.tableData.page = data.page;
        this.tableData.pageSize = data.pageSize;
        this.tableData.total = data.total;

        console.log('table data', this.tableData);
      });

    this.columnDefinitions = this.ColumnDefinition.filter(
      (item: any) => item.visible
    );
    let searchColumns = this.ColumnDefinition.filter(
      (item: any) => item.searchable
    );
    if (searchColumns.length > 0) {
      this.searchColumn = searchColumns[0].label;
    }

    console.log(this.columnDefinitions, this.searchColumn);
  }

  onChangePage(searchParams: SearchConfig) {
    this.lastSearchParams = Object.assign({}, searchParams);
    this.sportsService
      .getTeamPage(
        searchParams.page,
        searchParams.pageSize,
        searchParams.sortColumn,
        searchParams.sortDirection,
        searchParams.searchTerm
      )
      .subscribe((data: TeamPage) => {
        this.tableData.data = data.data;
        this.tableData.page = data.page;
        this.tableData.pageSize = data.pageSize;
        this.tableData.total = data.total;
      });
  }
}
