import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Sportsman } from '../../models/sportsman.interface';
import { SportsResortService } from '../../sports-resort.service';

@Component({
  selector: 'sports-resort',
  templateUrl: './sports-resort.component.html',
  styleUrls: ['./sports-resort.component.css'],
})
export class SportsResortComponent implements OnInit {
  currentuser: Sportsman;

  constructor(
    private router: Router,
    private sportsService: SportsResortService
  ) {}

  ngOnInit(): void {
    this.sportsService.getLoggedInUser().subscribe((data: Sportsman[]) => {
      //console.log('Subscribe: ', data);
      this.currentuser = data[0];
      console.log('Current user', this.currentuser);
      return;
    });
  }

  openLogin() {
    //console.log('open login');
    this.router.navigate(['/login']);
  }

  openSignUp() {
    this.router.navigate(['/signup']);
  }

  openCard() {
    this.router.navigate(['/sportscard']);
  }

  openRanking() {
    this.router.navigate(['/ranking']);
  }

  onLogout() {
    this.currentuser.loggedIn = false;
    //
    this.sportsService
      .updateUser(this.currentuser)
      .subscribe((upd: Sportsman) => {
        this.currentuser = upd;
        return;
      });
  }
}
