import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SportsResortComponent } from './containers/sports-resort/sports-resort.component';
import { AboutUsComponent } from './components/about-us/about-us.component';

import { SportsCardComponent } from './containers/sports-card/sports-card.component';
import { SportsActivityComponent } from './containers/sports-activity/sports-activity.component';

import { SportsGalleryComponent } from './containers/sports-gallery/sports-gallery.component';
import { SportsTeamComponent } from './containers/sports-team/sports-team.component';

const routes: Routes = [
  { path: 'sportsresort', component: SportsResortComponent },
  { path: 'aboutus', component: AboutUsComponent },

  { path: 'sportscard', component: SportsCardComponent },
  { path: 'activity', component: SportsActivityComponent },

  { path: 'gallery', component: SportsGalleryComponent },
  { path: 'team', component: SportsTeamComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SportsResortRoutingModule {}
