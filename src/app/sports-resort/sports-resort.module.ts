import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// components
import { SportsResortComponent } from './containers/sports-resort/sports-resort.component';
import { SportsResortHeader } from './components/header/sports-resort-header.component';
import { AboutUsComponent } from './components/about-us/about-us.component';

import { SportsCardComponent } from './containers/sports-card/sports-card.component';
import { SportsActivityComponent } from './containers/sports-activity/sports-activity.component';
import { ActivityCardComponent } from './components/activity-card/activity-card.component';

import { SportsGalleryComponent } from './containers/sports-gallery/sports-gallery.component';
import { SportsTeamComponent } from './containers/sports-team/sports-team.component';
import { TableComponent } from './components/table/table.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgChartsModule } from 'ng2-charts';

import { SharedModule } from '../shared/shared.module';

import { SportsResortRoutingModule } from './sports-resort-routing.module';

// services
import { SportsResortService } from './sports-resort.service';

@NgModule({
  declarations: [
    SportsResortComponent,
    SportsResortHeader,
    AboutUsComponent,

    SportsCardComponent,
    SportsActivityComponent,
    ActivityCardComponent,

    SportsGalleryComponent,
    SportsTeamComponent,
    TableComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,

    NgbModule,
    NgChartsModule,
    SharedModule,

    SportsResortRoutingModule,
  ],
  exports: [
    SportsResortComponent,
    SportsResortHeader,
    AboutUsComponent,

    SportsCardComponent,
    SportsActivityComponent,

    SportsGalleryComponent,
    SportsTeamComponent,
    TableComponent,
    NgChartsModule,
  ],
  providers: [SportsResortService],
})
export class SportsResortModule {}
