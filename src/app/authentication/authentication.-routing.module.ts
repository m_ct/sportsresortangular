import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SportsLoginComponent } from './containers/sports-login/sports-login.component';
import { SportsSignupComponent } from './containers/sports-signup/sports-signup-component';

const routes: Routes = [
  { path: 'login', component: SportsLoginComponent },
  { path: 'signup', component: SportsSignupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
