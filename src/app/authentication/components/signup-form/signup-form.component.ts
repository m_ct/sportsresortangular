import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Sportsman } from 'src/app/sports-resort/models/sportsman.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'signup-form',
  templateUrl: 'signup-form.component.html',
  styleUrls: ['signup-form.component.css'],
})
export class SignupFormComponent {
  @Input()
  detail: Sportsman;

  @Output()
  update: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router) {}

  handleSubmit(event: Sportsman, isValid: boolean | null) {
    if (isValid) {
      console.log('emitting', event);
      this.update.emit(event);
    }
  }

  onClose() {
    this.router.navigate(['/sportsresort']);
  }
}
