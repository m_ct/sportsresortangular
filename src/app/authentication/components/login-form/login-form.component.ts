import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Sportsman } from 'src/app/sports-resort/models/sportsman.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'login-form',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.css'],
})
export class LoginFormComponent {
  @Input()
  detail: Sportsman;

  @Output()
  update: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router) {}

  handleSubmit(event: Sportsman, isValid: boolean | null) {
    if (isValid) {
      console.log('emitting', event);
      this.update.emit(event);
    }
  }

  onClose() {
    this.router.navigate(['/sportsresort']);
  }

  onSignup() {
    this.router.navigate(['/signup']);
  }
}
