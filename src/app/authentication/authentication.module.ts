import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';

// components

import { SportsLoginComponent } from './containers/sports-login/sports-login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { SportsSignupComponent } from './containers/sports-signup/sports-signup-component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthenticationRoutingModule } from './authentication.-routing.module';

@NgModule({
  declarations: [
    SportsLoginComponent,
    LoginFormComponent,
    SportsSignupComponent,
    SignupFormComponent,
  ],
  imports: [CommonModule, FormsModule, NgbModule, AuthenticationRoutingModule],
  exports: [SportsLoginComponent, SportsSignupComponent],
  //providers: [SportsResortService],
})
export class AuthenticationModule {}
