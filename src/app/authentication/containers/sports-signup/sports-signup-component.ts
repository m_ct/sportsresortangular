import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Sportsman } from 'src/app/sports-resort/models/sportsman.interface';
import { SportsResortService } from '../../../sports-resort/sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { concatMap } from 'rxjs';

@Component({
  selector: 'sports-signup',
  templateUrl: 'sports-signup.component.html',
  styleUrls: ['sports-signup.component.css'],
})
export class SportsSignupComponent implements OnInit {
  currentuser: Sportsman;
  newUser: Sportsman = <Sportsman>{};

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {}

  onSubmitData(event: any) {
    console.log('onSubmitData ', event);

    this.sportsService
      .getUser(event.mail, event.pass)
      .pipe(
        concatMap((res: Sportsman[]) => {
          if (res.length == 0) {
            /// save new user
            const id: string = event.mail + event.pass;
            this.newUser.id = id;
            this.newUser.mail = event.mail;
            this.newUser.pass = event.pass;
            this.newUser.sportsmanName = event.userName;
            this.newUser.location = event.location;
            this.newUser.date = event.date;
            this.newUser.thoughts = event.msg;
            this.newUser.loggedIn = true;
            this.newUser.sports = [];
            this.newUser.total = 0;
          } else {
            // err msg
            this.toastr.error('SignUp failed! User already exists');
          }
          return this.sportsService.addUser(this.newUser);
        })
      )
      .subscribe((upd: Sportsman) => {
        if (upd.loggedIn == true) {
          this.router.navigate(['/sportsresort']);
        }
        console.log('loggedin usr: ', upd);
      });
  }
}
