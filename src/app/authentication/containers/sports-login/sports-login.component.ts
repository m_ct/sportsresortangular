import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Sportsman } from 'src/app/sports-resort/models/sportsman.interface';
import { SportsResortService } from '../../../sports-resort/sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { concatMap, tap } from 'rxjs';

@Component({
  selector: 'sports-login',
  templateUrl: 'sports-login.component.html',
  styleUrls: ['sports-login.component.css'],
})
export class SportsLoginComponent implements OnInit {
  newUser: Sportsman;

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {}

  onSubmitData(event: any) {
    console.log('onSubmitData ', event);

    this.sportsService
      .getUser(event.mail, event.pass)
      .pipe(
        tap((res) => console.log('First result', res)),
        concatMap((res: Sportsman[]) => {
          if (res.length == 0) {
            this.toastr.error('User not found!');
          } else {
            this.newUser = res[0];
            this.newUser.loggedIn = true;
            this.toastr.success('Login successfull!');
          }
          return this.sportsService.updateUser(this.newUser);
        })
      )
      .subscribe((upd: Sportsman) => {
        if (upd.loggedIn == true) {
          this.router.navigate(['/sportsresort']);
        }
        console.log('loggedin usr: ', upd);
      });
  }
}
