import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SportsRankingComponent } from './sports-ranking/sports-ranking.component';

const routes: Routes = [{ path: 'ranking', component: SportsRankingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RankingRoutingModule {}
