export class RankingTableConfig {
  static RankingTableConfig: any[];
  static ColumnDefinition(): any[] {
    return [
      {
        key: 'id',
        label: 'Id',
        dataType: 'string',
        visible: false,
        searchable: false,
      },
      {
        key: 'mail',
        label: 'Email',
        dataType: 'string',
        visible: false,
        searchable: false,
      },
      {
        key: 'pass',
        label: 'Password',
        dataType: 'string',
        visible: false,
        searchable: false,
      },
      {
        key: 'sportsmanName',
        label: 'Name',
        dataType: 'string',
        visible: true,
        searchable: false,
      },
      {
        key: 'location',
        label: 'Location',
        dataType: 'string',
        visible: true,
        searchable: true,
      },
      {
        key: 'date',
        label: 'Birth Date',
        dataType: 'date',
        visible: true,
        searchable: false,
      },
      {
        key: 'thoughts',
        label: 'Thoughts',
        dataType: 'string',
        visible: false,
        searchable: false,
      },
      {
        key: 'loggedIn',
        label: 'Logged in',
        dataType: 'boolean',
        visible: false,
        searchable: false,
      },
      {
        key: 'total',
        label: 'Km',
        dataType: 'number',
        visible: true,
        searchable: false,
        thClass: 'color-col',
        tdClass: 'color-col',
      },
      {
        key: 'rank',
        label: 'Rank',
        dataType: 'number',
        visible: true,
        searchable: false,
      },
      {
        key: 'sports',
        label: 'Sports',
        dataType: 'array',
        visible: false,
        searchable: false,
      },
    ];
  }
}
