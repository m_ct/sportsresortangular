import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Chart, ChartDataset, ChartOptions } from 'chart.js';

import {
  Sportsman,
  Sport,
  SportsmenPage,
} from '../../sports-resort/models/sportsman.interface';
import { SportsResortService } from '../../sports-resort/sports-resort.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { RankingTableConfig } from '../config/ranking-table.config';
import { SearchConfig } from '../../shared/dynamic-table/dynamic-table.component';
import { faTrophy } from '@fortawesome/free-solid-svg-icons';

interface SportChartData {
  data: number[];
  label: string;
}

@Component({
  selector: 'sports-ranking',
  templateUrl: 'sports-ranking.component.html',
  styleUrls: ['sports-ranking.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SportsRankingComponent implements OnInit {
  allSports: Sport[];
  sportChartData: SportChartData[] = [];

  tableUsers: SportsmenPage = <SportsmenPage>{};
  columnDefinitions: any[];
  searchColumn = '';

  trophyIcon = faTrophy;

  constructor(
    private router: Router,
    private sportsService: SportsResortService,
    private toastr: ToastrService
  ) {}

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public sportLabels: string[] = [];
  public barChartLabels = [''];

  public barChartLegend = true;

  ngOnInit(): void {
    //////////////////////////////////////////////////////////////////
    //// table data

    this.initTable();

    this.sportsService
      .getSportsmenPage(1, 4, 'total', 'desc', '')
      .subscribe((data: SportsmenPage) => {
        this.tableUsers = data;
        //this.tableUsers.users.forEach((user) => {
        //if (user.rank === 1) {
        // user.tdClass = 'color-red';
        //}
        //});
      });

    ///////////////////////////////////////////////////////////////////
    this.sportsService.getAllSports().subscribe((data: Sport[]) => {
      this.allSports = data;

      // fill labels
      for (var sp of this.allSports) {
        if (sp.sport == 'Running') {
          if (this.sportLabels.indexOf(sp.date.toString()) < 0) {
            this.sportLabels[this.sportLabels.length] = sp.date.toString();
          }
        }
      }
      // empty and fill again
      this.barChartLabels = [];
      this.sportLabels.sort((a, b) => a.localeCompare(b));
      this.barChartLabels = this.sportLabels;

      //console.log(this.barChartLabels);

      // fill bar data
      let lastUsr: string = '';
      let newUser: SportChartData = <SportChartData>{};
      for (var usr of this.allSports) {
        if (usr.idUser != lastUsr) {
          newUser = <SportChartData>{};
          newUser.data = [];
          newUser.label = usr.idUser;
          lastUsr = usr.idUser;

          let item = this.allSports.filter(
            (item) => item.idUser === lastUsr && item.sport == 'Running'
          );
          item.sort(function (a, b) {
            if (a.date < b.date) return -1;
            if (a.date > b.date) return 1;
            return 0;
          });

          //console.log('newUser', item);

          for (var chartLbl of this.barChartLabels) {
            var foundSport = item.find(
              (activity) => activity.date.toString() == chartLbl
            );
            if (foundSport) {
              newUser.data[newUser.data.length] = foundSport.value;
            } else {
              newUser.data[newUser.data.length] = 0;
            }
          }

          this.sportChartData[this.sportChartData.length] = newUser;
        }
      }

      return;
    });
  }

  private initTable() {
    this.columnDefinitions = RankingTableConfig.ColumnDefinition().filter(
      (item) => item.visible
    );
    let searchColumns = RankingTableConfig.ColumnDefinition().filter(
      (item) => item.searchable
    );
    if (searchColumns.length > 0) {
      this.searchColumn = searchColumns[0].label;
    }
  }

  onClose() {
    this.router.navigate(['/sportsresort']);
  }

  onChangePage(searchParams: SearchConfig) {
    this.sportsService
      .getSportsmenPage(
        searchParams.page,
        searchParams.pageSize,
        searchParams.sortColumn,
        searchParams.sortDirection,
        searchParams.searchTerm
      )
      .subscribe((data: SportsmenPage) => {
        this.tableUsers = data;
      });
  }
}
