import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
//import { HttpClientModule } from '@angular/common/http';
//import { FormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';

// components

import { SportsRankingComponent } from './sports-ranking/sports-ranking.component';

//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgChartsModule } from 'ng2-charts';

import { SharedModule } from '../shared/shared.module';

// services
//import { SportsResortService } from '../sports-resort/sports-resort.service';
import { RankingRoutingModule } from './ranking-routing.module';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [SportsRankingComponent],
  imports: [
    //NgbModule,
    //CommonModule,
    //HttpClientModule,
    //FormsModule,
    //RouterModule,

    NgChartsModule,
    SharedModule,
    RankingRoutingModule,
    CommonModule,
    FontAwesomeModule,
  ],
  exports: [SportsRankingComponent, NgChartsModule],
  //providers: [SportsResortService],
})
export class RankingModule {}
