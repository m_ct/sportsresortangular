import { Directive, EventEmitter, Input, Output } from '@angular/core';

export type SortDirection = 'asc' | 'desc' | '';

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '(click)': 'rotate()',
  },
  /*
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()',
  },
  */
})
export class NgbdSortableHeader {
  @Input() sortable: string = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    console.log(this.direction);
    switch (this.direction) {
      case '':
        this.direction = 'asc';
        break;
      case 'asc':
        this.direction = 'desc';
        break;
      case 'desc':
        this.direction = '';
        break;
      default:
        this.direction = '';
    }

    console.log(this.direction);
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}
