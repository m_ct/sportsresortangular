import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { SortEvent } from './sortable.directive';

export interface SearchConfig {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: string;
}

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css'],
})
export class DynamicTableComponent implements OnInit {
  @Input()
  items: any[];

  @Input()
  set headers(value: any[]) {
    this._headers = value;
  }
  _headers: any[];

  @Input() rowTemplate: TemplateRef<any>;

  @Input()
  total: number;

  @Input()
  canDelete = false;

  @Input()
  searchColumn: any;

  @Output()
  deleteItem: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  newSearch: EventEmitter<SearchConfig> = new EventEmitter<SearchConfig>();

  searchConfig: SearchConfig = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: '',
  };

  constructor() {}

  ngOnInit(): void {}

  getNextPage() {
    this.newSearch.emit(this.searchConfig);
  }

  changeSize(e: any) {
    this.newSearch.emit(this.searchConfig);
  }

  changeTerm(e: any) {
    this.newSearch.emit(this.searchConfig);
  }

  deleteRow(item: any) {
    this.deleteItem.emit(item);
  }

  onSort({ column, direction }: SortEvent) {
    this.searchConfig.sortColumn = column;
    this.searchConfig.sortDirection = direction;

    this.newSearch.emit(this.searchConfig);
  }
}
