import { DatePipe, DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataTableFormat',
})
export class DataTableFormatPipe implements PipeTransform {
  transform(value: any, columnDef: any): any {
    switch (columnDef.dataType) {
      case 'string':
        break;
      case 'number':
        let pipeNum = new DecimalPipe('ro-RO');
        value = pipeNum.transform(value, '1.0-0');
        break;
      case 'date':
        let pipeDate = new DatePipe('ro-RO', '+0200');
        value = pipeDate.transform(value, 'mediumDate');
        break;
      default:
    }

    return value;
  }
}
