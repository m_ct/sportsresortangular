import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbdSortableHeader } from './dynamic-table/sortable.directive';

import { SharedRoutingModule } from './shared-routing.module';

import { DynamicTableComponent } from './dynamic-table/dynamic-table.component';
import { DataTableFormatPipe } from './dynamic-table/data-table-format.pipe';

@NgModule({
  declarations: [
    NgbdSortableHeader,
    DynamicTableComponent,
    DataTableFormatPipe,
  ],
  imports: [CommonModule, FormsModule, NgbModule, SharedRoutingModule],
  exports: [DynamicTableComponent, DataTableFormatPipe],
  bootstrap: [DynamicTableComponent],
})
export class SharedModule {}
